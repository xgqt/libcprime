# LibCPrime
Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite.

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/libcprime/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* qt5-connectivity
* libnotify

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreBox.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).
