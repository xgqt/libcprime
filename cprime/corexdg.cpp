/*
	*
	* CoreXdg.cpp - Xdg Mimes implementation for CoreApps
	*
*/

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QUrl>
#include <QProcess>
#include <QStorageInfo>

#include "corexdg.h"
#include "filefunc.h"

/* We assume that the home partition is always mounted */
QString CoreXdg::homePartition = QStorageInfo( QDir::homePath() ).rootPath();


QString CoreXdg::home() {

	/* If the env variable HOME is set and its proper, good! */
	QString __home = QString::fromLocal8Bit( qgetenv( "HOME" ) );
	if ( access( __home.toLocal8Bit().data(), R_OK | X_OK ) == 0 )
		return __home + "/";

	/* Otherwise, we divine it from the user ID */
	struct passwd *pwd = getpwuid( getuid() );
	return QString::fromLocal8Bit( pwd->pw_dir ) + "/";
};

QString CoreXdg::xdgDefaultApp( QString mimeType ) {

	/*
		*
		* We have the appsForMimeList. Now we need to filter some applications out as per user's choice and get the default value
		* First check mimeapps.list/[Default Associations], then mimeapps.list/[Added Associations]. The entry corresponding to the mimetype in
		* the first case and the first entry in the second case are the user defaults.
		* If the mimetype is not listed, then check mimeinfo.cache
		*
		* Do the same for /usr/local/share/applications and /usr/share/applications
		*
	*/

	QStringList files;
	files << CoreXdg::home() + QString( "/.config/mimeapps.list" );
	files << CoreXdg::home() + QString( "/.local/share/applications/mimeapps.list" );
	files << CoreXdg::home() + QString( "/.local/share/applications/defaults.list" );
	files << CoreXdg::home() + QString( "/.local/share/applications/mimeinfo.cache" );

	files << QString( "/usr/local/share/applications/mimeapps.list" );
	files << QString( "/usr/local/share/applications/defaults.list" );
	files << QString( "/usr/local/share/applications/mimeinfo.cache" );

	files << QString( "/usr/share/applications/mimeapps.list" );
	files << QString( "/usr/share/applications/defaults.list" );
	files << QString( "/usr/share/applications/mimeinfo.cache" );

	QString defaultValue;
	Q_FOREACH( QString file, files ) {
		QSettings defaults( file, QSettings::NativeFormat );
		defaultValue = defaults.value( QString( "Default Applications/%1" ).arg( mimeType ) ).toString();
		if ( defaultValue.isEmpty() )
			defaultValue = defaults.value( QString( "Added Associations/%1" ).arg( mimeType ) ).toString();

		else
			break;

		if ( defaultValue.isEmpty() or defaultValue.isNull() )
			continue;

		else
			break;
	}

	return defaultValue;
};


/*
	*
	* CoreDesktopFile
	*
*/

CoreDesktopFile::CoreDesktopFile( QString filename ) {

	if ( filename.isEmpty() )
		return;

    mFileUrl = CoreXdgMime::instance()->desktopPathForName( filename );
    mDesktopName = CPrime::FileUtils::baseName( filename );

	if ( mFileUrl.startsWith( "/usr/share/applications" ) )
		mRank = 1;

	else if ( mFileUrl.startsWith( "/usr/local/share/applications" ) )
		mRank = 2;

	else if ( mFileUrl.startsWith( QDir::home().filePath( ".local/share/applications" ) ) )
		mRank = 3;

	QSettings s( mFileUrl, QSettings::NativeFormat );
	s.beginGroup( "Desktop Entry" );

	mName = s.value( "Name" ).toString();
	mGenericName = s.value( "GenericName", mName ).toString();
	mDescription = s.value( "Description", "" ).toString();
	mCommand = s.value( "Exec" ).toString();
	mExec = s.value( "TryExec", mCommand.split( " ", Qt::SkipEmptyParts ).value( 0 ) ).toString();

	if ( not CPrime::FileUtils::exists( mExec ) ) {
		Q_FOREACH( QString path, QString::fromLocal8Bit( qgetenv( "PATH" ) ).split( ":", Qt::SkipEmptyParts ) ) {
			if ( CPrime::FileUtils::exists( path + "/" + mCommand ) ) {
				mExec = path + "/" + mCommand;
				break;
			}
		}
	}

	mIcon = s.value( "Icon" ).toString();

	QStringList args = mCommand.split( " " );
	foreach( QString arg, args ) {
		if ( arg == "%f" or arg == "%u" ) {
			mMultiArgs = false;
			mTakesArgs = true;
			mParsedArgs << "<#COREARG-FILE#>";
		}

		else if ( arg == "%F" or arg == "%U" ) {
			mMultiArgs = true;
			mTakesArgs = true;
			mParsedArgs << "<#COREARG-FILES#>";
		}

		else if ( arg == "%i" ) {
			if ( !mIcon.isEmpty() )
				mParsedArgs << "--icon" << mIcon;
		}

		else if ( arg == "%c" )
			mParsedArgs << mName;

		else if ( arg == "%k" )
			mParsedArgs << QUrl( mFileUrl ).toLocalFile();

		else
			mParsedArgs << arg;
	}

	QRegExp mimeRx( "^MimeType=([.]+)$" );

	QFile desktop( mFileUrl );
	desktop.open( QFile::ReadOnly );
	QString line = QString::fromLocal8Bit( desktop.readLine() );		// Will always be [Desktop Entry]

	do {
		if ( line.startsWith( "MimeType=" ) )
			mMimeTypes = line.replace( "MimeType=", "" ).split( ";", Qt::SkipEmptyParts );

		if ( line.startsWith( "Categories=" ) )
			mCategories = line.replace( "Categories=", "" ).split( ";", Qt::SkipEmptyParts );

		line = QString::fromLocal8Bit( desktop.readLine() );
	} while ( not desktop.atEnd() );

	mVisible = not s.value( "NoDisplay", false ).toBool();
	mRunInTerminal = s.value( "Terminal", false ).toBool();

	if ( s.value( "Type" ).toString() == "Application" )
		mType = Application;

	else if ( s.value( "Type" ).toString() == "Link" )
		mType = Link;

	else if ( s.value( "Type" ).toString() == "Directory" )
		mType = Directory;

	if ( mName.count() and mCommand.count() )
		mValid = true;
};

bool CoreDesktopFile::startApplication() {

	if ( not mValid )
		return false;

	QProcess proc;
	return proc.startDetached( mExec, QStringList() );
};

bool CoreDesktopFile::startApplicationWithArgs( QStringList args ) {

	if ( not mValid )
		return false;

	QProcess proc;

	QStringList execList = parsedExec();
	QString exec = execList.takeFirst();

	if ( not args.count() ) {

		execList.removeAll( "<#COREARG-FILES#>" );
		execList.removeAll( "<#COREARG-FILE#>" );

		return QProcess::startDetached( exec, execList );
	}

	QStringList argList;
	if ( mTakesArgs ) {
		if ( mMultiArgs ) {
			Q_FOREACH( QString exeArg, execList ) {
				if ( exeArg == "<#COREARG-FILES#>" ) {
					if ( args.count() )
						argList << args;
				}

				else
					argList << exeArg;
			}
		}

		else {
            int idx = execList.indexOf( "<#COREARG-FILE#>" );
            argList << execList;
            argList.removeAt( idx );
			if ( args.count() ) {
				argList.insert( idx, args.takeAt( 0 ) );
				argList << args;
            }
		}
	}

	else {
		argList << execList;
		if ( args.count() )
			argList << args;
	}

	return QProcess::startDetached( exec, argList );
};

QString CoreDesktopFile::desktopName() const {

	return mDesktopName;
};

QString CoreDesktopFile::name() const {

	return mName;
};

QString CoreDesktopFile::genericName() const {

	return mGenericName;
};

QString CoreDesktopFile::description() const {

	return mDescription;
};

QString CoreDesktopFile::exec() const {

	return mExec;
};

QString CoreDesktopFile::command() const {

	return mCommand;
};

QString CoreDesktopFile::icon() const {

	return mIcon;
};

QStringList CoreDesktopFile::mimeTypes() const {

	return mMimeTypes;
};

QStringList CoreDesktopFile::categories() const {

	return mCategories;
};

QStringList CoreDesktopFile::parsedExec() const {
    return mParsedArgs;
};

int CoreDesktopFile::type() const{

	return mType;
};

int CoreDesktopFile::rank() const {

	return mRank;
};

bool CoreDesktopFile::visible() const {

	return mVisible;
};

bool CoreDesktopFile::runInTerminal() const {

	return mRunInTerminal;
};

bool CoreDesktopFile::isValid() const {

	return mValid;
};

QString CoreDesktopFile::desktopFileUrl() const {

	return mFileUrl;
};

/*
	*
	* CoreXdgMime
	*
*/

CoreXdgMime* CoreXdgMime::globalInstance = nullptr;

AppsList CoreXdgMime::appsForMimeType( QMimeType mimeType ) {

	AppsList appsForMimeList;
	QStringList mimeList = QStringList() << mimeType.name() << mimeType.allAncestors();
	QSet<QString> mimeSet( mimeList.begin(), mimeList.end() );

	Q_FOREACH( CoreDesktopFile app, appsList ) {
        QStringList mmTypes = app.mimeTypes();
		QSet<QString> temp( mmTypes.begin(), mmTypes.end() );
        QSet<QString> intersected = temp.intersect(mimeSet);//app.mimeTypes().toSet().intersect( mimeSet );
		if ( intersected.count() ) {
			if ( ( app.type() == CoreDesktopFile::Application ) and app.visible() )
					appsForMimeList << app;
		}
	}

	QString defaultName = CoreXdg::xdgDefaultApp( mimeType.name() );
	for( int i = 0; i < appsForMimeList.count(); i++ ) {
		if ( appsForMimeList.value( i ).desktopName() == CPrime::FileUtils::baseName( defaultName ) ) {
			appsForMimeList.move( i, 0 );
			break;
		}
	}

	return appsForMimeList;
};

QStringList CoreXdgMime::mimeTypesForApp( QString desktopName ) {

	QStringList mimeList;

	if ( not desktopName.endsWith( ".desktop" ) )
		desktopName += ".desktop";

	foreach( QString appDir, appsDirs ) {
		if ( QFile::exists( appDir + desktopName ) ) {
			mimeList << CoreDesktopFile( appDir + desktopName ).mimeTypes();
			break;
		}
	}

	return mimeList;
};

AppsList CoreXdgMime::allDesktops() {

	return appsList;
};

CoreDesktopFile CoreXdgMime::application( QString exec ) {

	AppsList list;
	Q_FOREACH( CoreDesktopFile app, appsList ) {
		if ( app.command().contains( exec, Qt::CaseSensitive ) )
			list << app;

		else if ( app.name().compare( exec, Qt::CaseInsensitive ) == 0 )
			list << app;
    }

	if ( not list.count() )
		return CoreDesktopFile();

	int rank = -1, index = -1;
	for( int i = 0; i < list.count(); i++ ) {
		if ( rank < list.value( i ).rank() ) {
			rank = list.value( i ).rank();
			index = i;
		}
	}

	/* Desktop file with the highest rank will be used always */
	return list.at( index );
};

QString CoreXdgMime::desktopPathForName( QString desktopName ) {

	if ( not desktopName.endsWith( ".desktop" ) )
		desktopName += ".desktop";

	if ( CPrime::FileUtils::exists( desktopName ) )
		return desktopName;

	Q_FOREACH( QString appDirStr, appsDirs ) {
		if ( CPrime::FileUtils::exists( appDirStr + "/" + desktopName ) ) {
			return appDirStr + "/" + desktopName;
		}
	}

	return QString();
};

CoreDesktopFile CoreXdgMime::desktopForName( QString desktopName ) {

	if ( not desktopName.endsWith( ".desktop" ) )
		desktopName += ".desktop";

	if ( CPrime::FileUtils::exists( desktopName ) )
		return CoreDesktopFile( desktopName );

	QString desktopPath;
	Q_FOREACH( QString appDirStr, appsDirs ) {
		if ( CPrime::FileUtils::exists( appDirStr + "/" + desktopName ) ) {
			desktopPath = appDirStr + "/" + desktopName;
			break;
		}
	}

	return CoreDesktopFile( desktopPath );
};

void CoreXdgMime::parseDesktops() {

	appsList.clear();
	foreach( QString appDirStr, appsDirs ) {
		QDir appDir( appDirStr );
		Q_FOREACH( QFileInfo desktop, appDir.entryInfoList( QStringList() << "*.desktop", QDir::Files ) ) {
			appsList << CoreDesktopFile( desktop.absoluteFilePath() );
		}
	}
};

CoreXdgMime* CoreXdgMime::instance() {

	if ( CoreXdgMime::globalInstance )
		return globalInstance;

	CoreXdgMime::globalInstance = new CoreXdgMime();
	globalInstance->parseDesktops();

	return CoreXdgMime::globalInstance;
};

void CoreXdgMime::setApplicationAsDefault( QString appFileName, QString mimetype ) {

	if ( QProcess::execute( "xdg-mime", QStringList() << "default" << appFileName << mimetype ) )
		qDebug() << "Error while setting" << appFileName << "as the default handler for" << mimetype;
};

CoreXdgMime::CoreXdgMime() {

	appsDirs << QDir::home().filePath( ".local/share/applications/" );
	appsDirs << "/usr/local/share/applications/" << "/usr/share/applications/";
	appsDirs << "/usr/share/applications/kde4/" << "/usr/share/gnome/applications/";
};

CoreDesktopFile CoreXdgMime::xdgDefaultApp( QMimeType mimeType ) {

	return appsForMimeType( mimeType ).value( 0 );
};

uint qHash( CoreDesktopFile &app ) {

	QString hashString;
	hashString += app.name();
	hashString += app.genericName();
	hashString += app.description();
	hashString += app.command();
	hashString += app.icon();
	hashString += app.mimeTypes().join( " " );
	hashString += app.categories().join( " " );

	return qChecksum( hashString.toLocal8Bit().data(), hashString.count() );
};
