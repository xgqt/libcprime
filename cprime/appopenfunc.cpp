/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
    * Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QProcess>
#include <QFileInfo>
#include <QWidget>
#include <QUrl>

#include "infofunc.h"
#include "cvariables.h"
#include "settingsmanage.h"
#include "appopenfunc.h"
#include "applicationdialog.h"
#include "corexdg.h"
#include "filefunc.h"

/* CoreXdgMime instance */
static CoreXdgMime *mimeHandler = CoreXdgMime::instance();

/*
 * Open app using QProcess with argument.
 * appName : Exact name
 */
void CPrime::AppOpenFunc::systemAppOpener(const QString &appName, const QStringList &arg)
{
    CoreDesktopFile df( appName );

    if ( not df.startApplicationWithArgs( arg ) ) {
        qDebug() << "func(systemAppOpener) : Something has gone wrong; App could not be opened with args:" << appName << arg;
        return;
    }

    qDebug() << "func(systemAppOpener) : Opening" << appName << "with args" << arg;
}

/*
 * Catagorize a path with the extention to open on specific app
 * path : Need a specific pathCPrimeCPrime
 * processOwner : Object which responsible to open the app
 */
void CPrime::AppOpenFunc::appOpenEngine(const QString &path, QObject *processOwner)
{
    Q_UNUSED(processOwner) // remove in future

    if ( !CPrime::FileUtils::exists(path) || path.count() == 0 ) {
        qDebug()<<  "Warning!!! File does not exist...";
        return;
    }

    /* Check if there exists an app to handle the file @path */
    CoreDesktopFile app = mimeHandler->xdgDefaultApp( mimeDb.mimeTypeForFile( path ) );

    if ( not app.isValid() )
        QProcess::startDetached( "xdg-open", QStringList() << QString( path ) );
    else
        app.startApplicationWithArgs( QStringList() << path );

    qDebug()<< "func(appOpenEngine) : Opening" << path;
}

/*
 * Open app (open the *.desktop file) based on category.
 */
void CPrime::AppOpenFunc::defaultAppEngine(CPrime::Category ctg, const QFileInfo &file, const QString keyword, QWidget *parent)
{
    settingsManage *sm = settingsManage::initialize();
    QString path( file.absoluteFilePath() );
    if (!parent) {
        qDebug() << "ERROR: Parent not exists.";
        return;
    }

    QString catStr;
    if (ctg == Category::FileManager) catStr = "FileManager";
    else if (ctg == Category::MetadataViewer) catStr = "MetadataViewer";
    else if (ctg == Category::SearchApp) catStr = "SearchApp";
    else if (ctg == Category::ImageEditor) catStr = "ImageEditor";
    else if (ctg == Category::Terminal) catStr = "Terminal";
    else if (ctg == Category::Renamer) catStr = "Renamer";

    qDebug() << "Category " << catStr;

    // selected default app name from settings.
    QString defaultApp = sm->value("CoreApps", catStr);

    if (!defaultApp.count() || defaultApp == "Nothing") {
        qDebug() << "No default app selected!!!\nSelect a default app to avoid this message.";
        QString appName = setDefaultApp(parent);
        if (!appName.count()) {
            qDebug() << "No default app selected!!!";
            return;
        } else {
            sm->setValue("CoreApps", catStr, appName);
            defaultApp = appName;
        }
    }

    // Fetch those have arguments.
    // Start them at first.
    if (ctg == Category::SearchApp) {
        CoreDesktopFile df = CoreDesktopFile(defaultApp);

        // sometimes the exce is binary path, so we clear out the path
        QString exec = CPrime::FileUtils::baseName( df.exec() );

        QProcess proc;
        if ( exec == "corehunt" ) {
            if ( keyword.isEmpty() ) {
                df.startApplicationWithArgs( QStringList() << "--path" << path );
            } else {
                df.startApplicationWithArgs( QStringList() << "--path" << path << "--pattern" << keyword );
            }
        } else if ( exec == "catfish" ) {
            if ( keyword.isEmpty() ) {
                df.startApplicationWithArgs( QStringList() << path );
            } else {
                df.startApplicationWithArgs( QStringList() << path << "--fulltext" << keyword );
            }
        } else {
            proc.startDetached( defaultApp, QStringList() << path << keyword );
        }
        qDebug() << defaultApp + " executing.";

        return;
    }

    else if (ctg == Category::Terminal) {
        CoreDesktopFile df = CoreDesktopFile(defaultApp);

        // sometimes the exce is binary path, so we clear out the path
        QString exec = CPrime::FileUtils::baseName( df.exec() );

        // Terminals which take --working-directory switch
        QStringList longwd = { "coreterminal", "gnome-terminal", "terminator", "mate-terminal", "xfce4-terminal" };

        // Terminals which take --workdir switch
        QStringList shortwd = { "qterminal", "konsole", "nbterminal" };

        // Terminals which do not accept work-dir switch
        QStringList badwd = { "xterm" };

        // Special baby of the group: LXTerminal which wants --working-directory=WD
        QProcess proc;
        if ( exec == "lxterminal" )
            df.startApplicationWithArgs( QStringList() << "--working-directory=" + path );
        else if ( longwd.contains( exec ) )
            df.startApplicationWithArgs( QStringList() << "--working-directory" << path );
        else if ( shortwd.contains( exec ) )
            df.startApplicationWithArgs( QStringList() << "--workdir" << path );
        else
            proc.startDetached( "xterm", QStringList() << "-e"  << QString( "cd %1 & %2" ).arg( path ).arg( "/bin/bash" ) );

        qDebug() << defaultApp + " executing.";
        return;
    }

    CoreDesktopFile df = CoreDesktopFile(defaultApp);
    df.startApplicationWithArgs(QStringList() << file.absoluteFilePath());

    qDebug() << defaultApp + " executing.";
}

QString CPrime::AppOpenFunc::setDefaultApp(QWidget *parent)
{
    // Select application in the dialog
    ApplicationDialog *dialog = new ApplicationDialog(parent);
    QString appName;
    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            appName = dialog->getCurrentLauncher();
        }
    } else {
        appName = "";
    }

    return appName;
}
