/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "pinit.h"
#include "ui_pinit.h"

#include <QTimer>
#include <QFileInfo>
#include <QIcon>

#include "cprime.h"
#include "filefunc.h"


pinit::pinit(QWidget *parent):QDialog(parent),ui(new Ui::pinit)
{
    ui->setupUi(this);

    setWindowIcon(QIcon::fromTheme("bookmark-new"));

    // set the requried folders
    CPrime::FileUtils::setupFileFolder(CPrime::FileFolderSetup::ConfigFolder);

    connect(ui->cancel, &QToolButton::clicked, this, &pinit::close);

    ui->pinSection->clear();
    ui->pinSection->addItems(pin.getPinSections());
    ui->done->setEnabled(false);

    pinmanage pm;
    pm.checkPins();
}

pinit::~pinit()
{
    delete ui;
}

void pinit::on_done_clicked()
{
    if (ui->pinName->text().count() == 0) {
        ui->done->setEnabled(false);
    }

    if (ui->pinName->text().count() != 0 && ui->pinSection->currentText().count() != 0) {
        accepted = true;
        QTimer::singleShot(100, this, SLOT(close()));
        // Function from utilities.cpp
        qDebug()<< "Pin Added at '" + ui->pinSection->currentText();
    }
}

void pinit::pinName_Changed()
{
    if (ui->pinName->text().count() > 0) {
        QString str = pin.checkingPinName(ui->pinSection->currentText(), ui->pinName->text());

        if (str.count() > 0) {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(false);
        } else {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(true);
        }
    } else {
        ui->done->setEnabled(false);
    }
}

void pinit::checkPath()
{
    QString str = pin.checkingPinPath(ui->pinSection->currentText(), ui->pinName->text());

    if (str.count() > 0) {
        ui->pinStatus->setText(str);
        ui->pinName->setEnabled(false);
        ui->done->setEnabled(false);
        ui->cancel->setText("OK");
    } else {
        ui->pinStatus->setText(str);
        ui->pinName->setEnabled(true);
        ui->done->setEnabled(true);
        ui->cancel->setText("Cancel");
    }
}

void pinit::setPinPath(const QString &path)
{
    ui->path->setText(path);
}

void pinit::setPinName(const QString &bName)
{
    ui->pinName->setText(bName);
}

QString pinit::getPinName()
{
    return ui->pinName->text();
}

QString pinit::getSectionName()
{
    return ui->pinSection->currentText();
}

void pinit::item_Changed()
{
    checkPath();
    pinName_Changed();
}


void pinit::on_pinName_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1)

    if (ui->pinName->text().count() > 0) {
        QString str = pin.checkingPinName(ui->pinSection->currentText(), ui->pinName->text());

        if (str.count() > 0) {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(false);
        } else {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(true);
        }
    } else {
        ui->done->setEnabled(false);
    }
}

void pinit::on_pinSection_currentIndexChanged(const QString &arg1)
{
    Q_UNUSED(arg1)
    checkPath();
    pinName_Changed();
}

/**
 * @brief Call add pinit dialog from any apps by giving the path to pin it.
 * @brief And also the apps icon.
 * @param currentPath : Path which needs to be pinned.
 * @return Whether the pin added successfully.
 */
bool pinit::callPinIt(QWidget *parent, const QString &currentPath)
{
    QFileInfo info(currentPath);
    pinmanage pm;
    const QString str = pm.checkingPinPathEx(currentPath);

    if (str.count() == 0) {
        pinit *pinIt = new pinit(parent);
        QIcon ico = CPrime::ThemeFunc::getFileIcon(currentPath);
        QPixmap pix = ico.pixmap(QSize(100, 80));
        pinIt->setPinPath(currentPath);
        pinIt->setPinName(info.fileName() + "");
        pinIt->checkPath();

        if (pinIt->exec() == 0) {
			pinmanage pin;
            if (pinIt->accepted) {
                return pin.addPin(pinIt->getSectionName(), pinIt->getPinName(), currentPath);
            } else if (!pinIt->accepted) {
                pinIt->close();
            }
        }

        //sectionRefresh();
    } else {
        CPrime::InfoFunc::messageEngine("CorePins", "corepins", "Can't pin selected file(s)", str, parent);
    }

    return false;
}
