/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QWidget>
#include <QSettings>
#include <QFile>
#include <QDir>
#include <QSet>
#include <QList>
#include <QTextStream>
#include <QLineEdit>
#include <QDateTime>

#include "filefunc.h"
#include "pinmanage.h"


void pinmanage::createPinsFolder()
{
    CPrime::FileUtils::setupFileFolder(CPrime::FileFolderSetup::ConfigFolder);

    QFile file(cpinFullPath);
    file.open(QFile::ReadWrite);
    file.close();
}

void pinmanage::checkPins()
{
    QFile file(cpinFullPath);

    if (!file.exists()) {
        createPinsFolder();
    }

    file.open(QFile::ReadOnly);
    QString s = file.readAll();
    file.close();

    if (s.count() == 0) {
        QTextStream out(&file);
        file.open(QFile::WriteOnly);
        out << QString("[Speed%20Dial]\n~~~count~=0");
        file.close();
    }
}

QStringList pinmanage::getPinSections()
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    return bkGet.childGroups();
}

QStringList pinmanage::getPinNames(QString sectionName)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);

    bkGet.beginGroup(sectionName);
    QStringList list = bkGet.allKeys();
    bkGet.endGroup();
    list.removeOne("~~~count~");
    return list;
}

bool pinmanage::addSection(QString sectionName)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);

    if (!getPinSections().contains(sectionName, Qt::CaseInsensitive)) {
        bkGet.beginGroup(sectionName);

        for (int i = 0; i < getPinSections().count(); ++i) {
            if (sectionName.contains(getPinSections().at(i), Qt::CaseInsensitive) == false) {
                bkGet.setValue("~~~count~", "0");
            }
        }

        bkGet.endGroup();
        return true;
    }

    return false;
}

bool pinmanage::addPin(QString sectionName, QString pinName, QString pinpath)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);

    if (!getPinNames(sectionName).contains(pinName, Qt::CaseInsensitive)) {
        bkGet.beginGroup(sectionName);

        for (int i = 0; i < getPinNames(sectionName).count(); ++i) {
            if (pinName.contains("~~~count~", Qt::CaseInsensitive) == true) {
                return false;
            } else {
                if (pinName.contains(getPinNames(sectionName).at(i), Qt::CaseInsensitive) == false) {
                    bkGet.setValue("~~~count~", bkGet.childKeys().count());
                }
            }
        }

        bkGet.setValue(pinName, pinpath + "\t\t\t" + QDateTime::currentDateTime().toString("hh.mm.ss - dd.MM.yyyy"));
        bkGet.endGroup();
        return true;
    }

    return false;
}

void pinmanage::delSection(QString sectionName)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    bkGet.remove(sectionName);
}

void pinmanage::delPin(QString pinName)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    bkGet.remove(pinName);
}
void pinmanage::delPin(QString pinName, QString section)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    bkGet.beginGroup(section);
    bkGet.remove(pinName);
    bkGet.setValue("~~~count~", bkGet.childKeys().count());
    bkGet.endGroup();
}

void pinmanage::editPin(QString sectionName, QString pinName, QString pinpath)
{
    addPin(sectionName, pinName, pinpath);
}

void pinmanage::changeAll(QString oldSectionName, QString oldpinName, QString sectionName, QString pinName, QString pinValue)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    delPin(oldpinName, oldSectionName);
    addPin(sectionName, pinName, pinValue);
}

void pinmanage::changeSection(QString oldSectionName, QString sectionName, QString pinName, QString pinValue)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);

    bkGet.beginGroup(oldSectionName);
    bkGet.remove(pinName);
    bkGet.setValue("~~~count~", bkGet.childKeys().count());
    bkGet.endGroup();

    bkGet.beginGroup(sectionName);
    bkGet.setValue(pinName, pinValue);
    bkGet.setValue("~~~count~", bkGet.childKeys().count());
    bkGet.endGroup();
}

void pinmanage::changePin(QString oldpinName, QString sectionName, QString pinName, QString pinValue)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    bkGet.remove(oldpinName);

    bkGet.beginGroup(sectionName);
    bkGet.setValue(pinName, pinValue);
    bkGet.endGroup();
}

QString pinmanage::pinValues(QString sectionName, QString pinName)
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);

    bkGet.beginGroup(sectionName);
    return bkGet.value(pinName).toString();
}

QString pinmanage::pinPath(QString sectionName, QString pinName)
{
    QStringList values(pinValues(sectionName, pinName).split("\t\t\t"));
    return values.at(0);
}

QString pinmanage::piningTime(QString sectionName, QString pinName)
{
    QStringList values(pinValues(sectionName, pinName).split("\t\t\t"));
    return values.at(1);
}

QString pinmanage::checkingPinName(QString sectionName, QString pinName)
{
    if (getPinNames(sectionName).contains(pinName, Qt::CaseInsensitive)) {
        return "Pin exists.";
    } else {
        return "";
    }
}

QString pinmanage::keyCount()
{
    return QString("%1").arg(keys().count());
}

QStringList pinmanage::keys()
{
    QSettings bkGet(cpinFullPath, QSettings::IniFormat);
    QStringList list;

    for (int i = 0; i < bkGet.allKeys().count(); ++i) {
        list.append(QString(bkGet.allKeys().at(i)).split("/").at(1));
    }

	list = QSet<QString>(list.begin(), list.end()).values();
    list.removeOne("~~~count~");
    return list;
}

// Check on specific section
QString pinmanage::checkingPinPath(QString sectionn, QString pinpath)
{
    QLineEdit *line = new QLineEdit();

    foreach (QString bName, getPinNames(sectionn)) {
        if (!QString::compare(pinPath(sectionn, bName), pinpath, Qt::CaseSensitive)) {
            line->setText("Path exists in this section.");
            return line->text();
        } else {
            line->setText("");
            continue;
        }
    }

    return line->text();
}

// Check all section
QString pinmanage::checkingPinPathEx(QString pinpath)
{
    QLineEdit *line = new QLineEdit();

    foreach (QString section, getPinSections()) {
        foreach (QString bName, getPinNames(section)) {
            if (!QString::compare(pinPath(section, bName), pinpath, Qt::CaseSensitive)) {
                line->setText(QString("\"%1\" \nexists in \"%2\" section.").arg(bName).arg(section));
                return line->text();
            } else {
                line->setText("");
                continue;
            }
        }
    }

    return line->text();
}
