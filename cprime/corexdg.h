/*
	*
	* CoreXdg.hpp - CoreXdg class header
	*
*/

#pragma once

#include <QMimeDatabase>
#include <QMimeType>

#include "cenums.h"
#include "libcprime_global.h"

static QMimeDatabase mimeDb;


class LIBCPRIMESHARED_EXPORT CoreXdg {

public:
    enum XdgUserDir {
        XDG_DATA_HOME    = 0x02544878,
        XDG_CONFIG_HOME,
        XDG_CACHE_HOME
    };

    enum XdgSystemDirs {
        XDG_DATA_DIRS    = 0x196BB115,
        XDG_CONFIG_DIRS
    };

    static QString home();
    static QString xdgDefaultApp( QString );
    static QString userDir( CoreXdg::XdgUserDir );
    static QStringList systemDirs( CoreXdg::XdgSystemDirs );

    static QString trashLocation( QString path );
    static QString homeTrashLocation();

    static QString homePartition;

};

class LIBCPRIMESHARED_EXPORT CoreDesktopFile {

public:
    enum Type {
        Application = 0x906AF2,							// A regular executable app
        Link,											// Linux equivalent of '.lnk'			! NOT HANDLED
        Directory										// Desktopthat points to a directory	! NOT HANDLED
    };

    CoreDesktopFile( QString filename = QString() );	// Create an instance of a desktop file

    bool startApplication();
    bool startApplicationWithArgs( QStringList );

    QString desktopName()const;							// Filename of the desktop
    QString name() const;								// Name
    QString genericName() const;						// Generic Name
    QString description() const;						// Comment
    QString exec() const;								// 'TryExec' value or the path divined from 'Exec'
    QString command() const;							// Full command as given in 'Exec'
    QString icon() const;								// Application Icon Name or Path

    QStringList mimeTypes() const;						// MimeTypes handled by this app
    QStringList categories() const;						// Categories this app belongs to
    QStringList parsedExec() const;						// Arguments parsed with %U, %F, %u, %f etc removed

    int type() const;									// Application/Link/Directory
    int rank() const;

    bool visible() const;								// Visible in 'Start' Menu
    bool runInTerminal() const;							// If this app should be run in the terminal
    bool multipleArgs() const;							// Does the app take multiple arguments?
    bool isValid() const;								// Is a valid desktop file

    QString desktopFileUrl() const;						//

private:
    QString mFileUrl, mDesktopName, mExec, mCommand;
    QString mName, mGenericName, mDescription, mIcon;
    QStringList mMimeTypes, mCategories, mParsedArgs;

    bool mVisible, mRunInTerminal, mValid = false;
    bool mMultiArgs = false, mTakesArgs = false;

    int mType;

    short int mRank = 3;

};

typedef QList<CoreDesktopFile> AppsList;

class LIBCPRIMESHARED_EXPORT CoreXdgMime {

public:
    static CoreXdgMime* instance();

    // Get a list of applications for a mime type given
    AppsList appsForMimeType( QMimeType );

    // Get a list of mimetypes an application handles, given the desktop name
    QStringList mimeTypesForApp( QString );

    // List all the applications
    AppsList allDesktops();

    // Get the consolidated/unified application file for a desktop name
    CoreDesktopFile application( QString );

    // Get the best desktop file path for a desktop name
    QString desktopPathForName( QString );

    // Get the best desktop file path for a desktop name
    CoreDesktopFile desktopForName( QString );

    // Add one new application location
    void addAppsLocations( QString );

    // Add multiple applications locations
    void addAppsLocations( QStringList );

    // Parse all desktops
    void parseDesktops();

    static void setApplicationAsDefault( QString, QString );

    CoreDesktopFile xdgDefaultApp( QMimeType );

private:
    CoreXdgMime();

    static CoreXdgMime *globalInstance;

    QStringList appsDirs;
    AppsList appsList;
};

uint qHash( const CoreDesktopFile &app );
Q_DECLARE_METATYPE( CoreDesktopFile );
