/*
    *
    * This file is a part of Libcprime
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QDBusPendingReply>

#include "corenotifier.h"

/*
	*
	* Notifier Interface
	*
*/

#define INTERFACE "org.freedesktop.Notifications"
#define SERVICE "org.freedesktop.Notifications"
#define PATH "/org/freedesktop/Notifications"

NotifierInterface::NotifierInterface() : QDBusAbstractInterface( SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus(), nullptr ) {
};

/*
	*
	* CoreNotifier
	*
*/

CoreNotifier *CoreNotifier::mInstance = nullptr;

CoreNotifier* CoreNotifier::instance() {

	if ( not mInstance )
		mInstance = new CoreNotifier();

	return mInstance;
};

CoreNotifier::CoreNotifier() : QObject() {

	iface = new NotifierInterface();
	init = true;
};

void CoreNotifier::showNotification( QString appName, QString iconName, QString title, QString details, int timeout ) {

	/* The third argument (= 0) is the id that needs to be replaced, 0 means, no replacements. */

	QDBusPendingCall reply = iface->asyncCall( "Notify", appName, ( quint32 )0, iconName, title, details, QStringList(), QVariantMap(), timeout );
	QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher( reply, this );

	connect( watcher, SIGNAL( finished( QDBusPendingCallWatcher* ) ), this, SLOT( handleErrors( QDBusPendingCallWatcher* ) ) );
};

void CoreNotifier::handleErrors( QDBusPendingCallWatcher *watcher ) {

	 QDBusPendingReply<quint32> reply = *watcher;

	if ( reply.isError() ) {
		qDebug() << reply.error().message();
		emit notificationNotShown();
	}

	watcher->deleteLater();
};
