/*
    *
    * This file is a part of Libcprime
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QString>
#include <QWidget>
#include <QtPlugin>
#include <QListWidget>
#include <QIcon>
#include "cvariables.h"

class LIBCPRIMESHARED_EXPORT plugininterface : public QObject {

	Q_OBJECT

public:
	virtual ~plugininterface() {}

	/* Name of the plugin */
	virtual QString name() = 0;

	/* The plugin version */
	virtual QString version() = 0;

	/* The widget */
	virtual QWidget *widget(QWidget *parent) = 0;

};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(plugininterface, COREACTION_PLUGININTERFACE/*"com.coreaction.plugininterface"*/)
QT_END_NAMESPACE


class LIBCPRIMESHARED_EXPORT ShareItInterface : public QObject {

public:
	enum Context {
		File         = 0x929C29,
		Files,
		Dir,
		Dirs,
		All
	};

	/* Name of the plugin */
	virtual QString name() = 0;

	/* Icon of the plugin */
	virtual QIcon icon() = 0;

	/* MimeTypes handled by the plugin */
	virtual QStringList mimeTypes() = 0;

	/* Context of the plugin */
	virtual int context() = 0;

	/* Dialog to be shown */
	virtual bool shareItDialog(QStringList files, QWidget *parent) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(ShareItInterface, CPRIME_PLUGININTERFACE/*"com.coreaction.shareitinterface"*/)
QT_END_NAMESPACE

class LIBCPRIMESHARED_EXPORT TasksPlugin : public QObject {
	Q_OBJECT

public:
	virtual ~TasksPlugin() {}

	virtual void init() = 0;

	/* Target of the plugin */
	virtual void setBackendTarget(QListWidget *) = 0;

	/* Update the list of clients */
	virtual void updateClientList() = 0;

	/* Show/unshow the desktop */
	virtual void showDesktop() = 0;

	/* Show/unshow the desktop */
	virtual QSize desktopSize() = 0;

Q_SIGNALS:
	/* Signal for corestuff to resize to occupy full available space */
	void resizeDesktop();

	/* Signal for corestuff to activate the dock button */
	void activateDock();
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(TasksPlugin, TASKS_PLUGININTERFACE/*"com.corestuff.tasksplugin"*/)
QT_END_NAMESPACE
