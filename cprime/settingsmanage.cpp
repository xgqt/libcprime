/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDir>
#include <QFontDatabase>
#include <QIcon>
#include <QFileInfo>
#include <QProcess>
#include <QStandardPaths>

#include <unistd.h>

#include "cvariables.h"
#include "settingsmanage.h"


QSettings* settingsManage::cSetting = nullptr;
settingsManage *settingsManage::mSettingsManage = nullptr;

settingsManage::settingsManage(bool checkIndividual)
{
    cSetting = new QSettings("coreapps", "coreapps");
    qDebug() << "Settings file " << cSetting->fileName();

    QString defaultSett = CPrime::Variables::CC_CoreApps_Default_ConfigFilePath();
    QString userSett = cSetting->fileName();

    // set some defult settings that are user specific
    if (!QFileInfo(cSetting->fileName()).exists()) {
        qDebug() << "Creating settings path : " << QDir().mkpath(QFileInfo(userSett).absolutePath());
        qDebug() << "Default settings path : " << defaultSett;
        qDebug() << "User settings path : " << userSett;
        qDebug() << "Copying default Settings : " << QFile(defaultSett).copy(userSett);

        setUserDefultSettings();

    } else {
        if (checkIndividual) {
            QSettings defSett(defaultSett, QSettings::NativeFormat);

            Q_FOREACH (QString key, defSett.allKeys()) {
                if (!cSetting->allKeys().contains(key)) {
                    cSetting->setValue(key, defSett.value(key));
                }
            }

            Q_FOREACH (QString key, cSetting->allKeys()) {
                if (!defSett.allKeys().contains(key)) {
                    cSetting->remove(key);
                }
            }
        }
    }
}

settingsManage *settingsManage::initialize(bool checkIndividual)
{	
	if (!geteuid()) {
		qDebug() << "Rejecting settings file creation while as root user.";
		return nullptr;
	}

    if (mSettingsManage) {
        return mSettingsManage;
    }

    mSettingsManage = new settingsManage(checkIndividual);
    return mSettingsManage;
}

void settingsManage::setUserDefultSettings()
{
    // Save user home directory
    if (!cSetting->value("CoreFM/StartupPath").toString().count()) {
        cSetting->setValue("CoreFM/StartupPath", QDir::homePath());
    }

    // Save screenshot directory
    if (!cSetting->value("CoreShot/SaveLocation").toString().count()) {
        QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/Screen Shots");
        cSetting->setValue("CoreShot/SaveLocation", QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/Screen Shots");
    }

    // As default settings not exist we should contain to set value for default
    // Add system font to CorePad, CoreTerminal, CoreKeyboard
    QFont genFont = QFontDatabase::systemFont(QFontDatabase::GeneralFont);
    QFont monoFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    if ( not genFont.family().count() )
        genFont = QFont("Cantarell", 9);

    if ( not monoFont.family().count() )
        monoFont = QFont("monospace", 9);

    if (monoFont.styleHint() != QFont::Monospace) {
        monoFont = QFont("monospace", 9);
    }

    cSetting->setValue("CorePad/Font", monoFont);
    cSetting->setValue("CoreTerminal/Font", monoFont);
    cSetting->setValue("CoreKeyboard/Font", genFont.family());

    // Set keyboard layout to mobile and mode to fixed while touch mode detected
    bool touch = value("CoreApps", "TouchMode");
    if (touch) {
        setValue("CoreKeyboard", "Layout", 0);
        setValue("CoreKeyboard", "Mode", 0);
        setValue("CoreApps", "SideViewIconSize", QSize(48, 48));
        setValue("CoreApps", "IconViewIconSize", QSize(56, 56));
        setValue("CoreApps", "ListViewIconSize", QSize(48, 48));
        setValue("CoreApps", "ToolBarIconSize", QSize(48, 48));
    } else {
        setValue("CoreKeyboard", "Layout", 1);
        setValue("CoreKeyboard", "Mode", 1);
        setValue("CoreApps", "SideViewIconSize", QSize(24, 24));
        setValue("CoreApps", "IconViewIconSize", QSize(48, 48));
        setValue("CoreApps", "ListViewIconSize", QSize(32, 32));
        setValue("CoreApps", "ToolBarIconSize", QSize(24, 24));
    }
}


void settingsManage::reload()
{
    cSetting->sync();
}

QStringList settingsManage::getKeys(const QString &appName)
{
    cSetting->beginGroup(appName);
    QStringList keys = cSetting->childKeys();
    cSetting->endGroup();
    return keys;
}

settingsManage::cProxy settingsManage::value(const QString &appName, const QString &key, QVariant defaultValue)
{
    cSetting->sync();		//Better than calling reload()

    if ( ( appName == "CoreApps" ) and ( key == "TouchMode" ) ) {
		int touch = cSetting->value( appName + "/" + key ).toInt();
		switch ( touch ) {
			/* Auto detect */
			case 0: {
				QProcess proc;
				proc.start( "udevadm", QStringList() << "info" << "--export-db" );
				proc.waitForFinished( -1 );

				QString output = QString::fromLocal8Bit( proc.readAllStandardOutput() + '\n' + proc.readAllStandardError() );
				if ( output.contains( "ID_INPUT_TOUCHSCREEN=1" ) )
					return cProxy{ "Dummy", true };

				return cProxy{ "Dummy", false };
			}

			/* TouchMode is on */
			case 1:
				return cProxy{ "Dummy", true };

			/* TouchMode is off */
			case 2:
				return cProxy{ "Dummy", false };

			default:
				return cProxy{ "Dummy", false };
        }
	}

	else if ( ( appName == "CoreApps" ) and ( key == "TouchModeRaw" ) ) {
		return cProxy{ "Dummy", cSetting->value( appName + "/TouchMode" ).toInt() };
	}

    return cProxy{ appName + "/" + key, defaultValue };
}

void settingsManage::setValue(const QString &appName, const QString &key, QVariant value)
{
    cSetting->setValue(appName + "/" + key, value);
}
