/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QApplication>
#include <QFileInfo>
#include <QFile>
#include <QWidget>
#include <QIcon>
#include <QStyle>
#include <QPainter>
#include <QDir>

#include "corexdg.h"
#include "filefunc.h"
#include "cvariables.h"
#include "themefunc.h"

/*
 * Extract Icon from *.desktop file.
 */
QIcon CPrime::ThemeFunc::getAppIcon(const QString &appName)
{
	/*
		* We should not struggle so hard to search for the icons. Either it will be an absolute path
		* or a name. So our first attempt should be just QIcon::fromTheme( icoStr, QIcon( icoStr ) )
		* This will cover the standard icon specifications. Second attempt will be just see if
		* @icoStr exists in /usr/share/pixmaps. If not, return
		* QIcon::fromTheme( "application-x-executable" )
	*/

	CoreDesktopFile df = CoreXdgMime::instance()->desktopForName( appName );
	if ( df.isValid() ) {
		QString icoStr = df.icon();

		/* First attempt */
		if ( CPrime::FileUtils::exists( icoStr ) or QIcon::hasThemeIcon( icoStr ) )
			return QIcon::fromTheme( icoStr, QIcon( icoStr ) );

		/* Second attempt */
		if ( CPrime::FileUtils::exists( "/usr/share/pixmaps/" + icoStr ) )
			return QIcon( "/usr/share/pixmaps/" + icoStr );
	}

	/* Fallback */
	qDebug() << appName << "Using fallback icon: application-x-executable";
	return QIcon::fromTheme( "application-x-executable" );
}

/*
 * Extract Icon from mime database.
 */
QIcon CPrime::ThemeFunc::getFileIcon(const QString &filePath)
{
    QFileInfo info(filePath);

    if (filePath.count() == 0 || !info.exists()) {
        return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
    }

    QIcon icon;
    QMimeDatabase mimedb;
    QMimeType mimeType;

    mimeType = mimedb.mimeTypeForFile(filePath);
    icon = QIcon::fromTheme(mimeType.iconName());

    if (icon.isNull()) {
        return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
    }

    return icon;
}

QString CPrime::ThemeFunc::getSideViewStyleSheet()
{
    QString path = QString("%1").arg(COREAPPS_THEME_FOLDER) + "/sideview.qss";

    QFile file(path);
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());


    return styleSheet;
}

QIcon CPrime::ThemeFunc::themeIcon( QString name1, QString name2, QString stock )
{
    if ( QIcon::hasThemeIcon( name1 ) )
        return QIcon::fromTheme( name1, QIcon( stock ) );

    else if ( QIcon::hasThemeIcon( name2 ) )
        return QIcon::fromTheme( name2, QIcon( stock ) );

    else
        return QIcon( stock );
}

QIcon CPrime::ThemeFunc::resizeIcon(QIcon originalIcon, QSize containerSize)
{
    QIcon resized = originalIcon;

    if (!resized.pixmap(48).width()) {
        resized = QIcon::fromTheme("application-x-executable");
    } else {
        QSize iSize = containerSize;
        QPixmap iPix = resized.pixmap(iSize);
        QSize actualSize = iPix.size();
        if ((actualSize.width() < iSize.width()) || actualSize.height() < iSize.height()) {
            QPixmap pix(iSize);
            pix.fill(Qt::transparent);

            QPainter painter(&pix);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);

            QRect iconRect(QPoint((iSize.width() - actualSize.width()) / 2, (iSize.height() - actualSize.height()) / 2), actualSize);
            painter.drawPixmap(iconRect, iPix);

            resized = QIcon(pix);
        }
    }

    return resized;
}
