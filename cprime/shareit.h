/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include <QDialog>
#include <QListWidget>

#include "cplugininterface.h"
#include "settingsmanage.h"

#include "libcprime_global.h"

class QListWidgetItem;

namespace Ui {
    class shareit;
}

class LIBCPRIMESHARED_EXPORT shareit : public QDialog {

    Q_OBJECT

public:
    enum Type {
        Plugins = 1,
        Apps = 2,
        Folders = 3
    };
    Q_ENUM(Type)

    explicit shareit(QStringList files, QWidget *parent = nullptr);
    ~shareit();

    static void shareFiles(QWidget *parent, QStringList files);

private slots:
    void folders_itemActivated(QListWidgetItem *item);
    void apps_itemActivated(QListWidgetItem *item);
    void plugin_itemActivated(QListWidgetItem *item);
    void copyToClipboard(QListWidgetItem *item);
    void openFileLocation(QListWidgetItem *item);

private:
    Ui::shareit *ui;
    QListWidget *pluginWidget, *otherOptions;
    QStringList mFiles;
    QList<ShareItInterface *> plugins;
    settingsManage *sm = settingsManage::initialize();
    QString shareSelection;

    QWidget *mParent;
    void populateFolders();
    QString toDriveFolder(QString driveMountPoint);
    void populatePlugins();
    void populateFavouriteApps();
    void itemSelected(QListWidgetItem *);
};
