/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QWidget>
#include <QLabel>
#include <QTimer>
#include <QVBoxLayout>
#include <QDateTime>
#include <QApplication>
#include <QScreen>
#include <QDir>
#include <QSettings>

#include "settingsmanage.h"
#include "infofunc.h"
#include "sortfunc.h"
#include "cvariables.h"
#include "corenotifier.h"

/*
 * MessageBox function.
 */
void CPrime::InfoFunc::messageEngine(const QString &appName, const QString &appIcon, const QString &summary,
                                     const QString &body, QWidget *parent)
{
    settingsManage *sm = settingsManage::initialize();

    if(sm->value("CoreApps", "UseSystemNotification")){
        /*
			*
			* CoreNotifier class gives out a signal if it fails to show the notification.
			* It can be used to show our custom notification:
			*
			* CoreNotifier *notifier = CoreNotifier::instance();
			* connect( notifier, &CoreNotifier::notificationNotShown, ()[=] {
			*		// Code to show our custom notification
			* } );
			*
			* notifier->showNotification( appName, appIcon, summary, body, 2000 );
			*
		*/
        CoreNotifier::instance()->showNotification( appName, appIcon, summary, body, 2000 );
    } else {
        QWidget *mbox = new QWidget(parent);
        QLabel *l = new QLabel( appName + "\n" + body);
        QVBoxLayout *bi = new QVBoxLayout();

        mbox->setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::ToolTip);
        mbox->setMinimumSize(230, 50);
        mbox->setLayout(bi);
        l->setStyleSheet("QLabel { padding: 10px; }");
        l->setAlignment(Qt::AlignCenter);
        bi->addWidget(l);
        bi->setContentsMargins(0, 0, 0, 0);

        mbox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        mbox->setStyleSheet("QWidget { background-color: rgba(35, 35, 35, 200); color: #ffffff; border: 1px #2A2A2A; border-radius: 3px; }");
        mbox->show();

        QScreen *scr = QGuiApplication::primaryScreen();
        int x = scr->availableGeometry().width() - (mbox->width() + 5);
        int y = scr->availableGeometry().height() - (mbox->height() + 5);
        mbox->move(x, y);

        QTimer::singleShot(3000, mbox, SLOT(close()));
    }
}

QString CPrime::InfoFunc::sentDateText(const QString &dateTime)
{
    QDateTime given = QDateTime::fromString(dateTime, "dd.MM.yyyy");

    if (QDate::currentDate().toString("dd.MM.yyyy") == dateTime) {
        return QString("Today");
    } else {
        return QString(given.toString("MMMM dd"));
    }
}

bool CPrime::InfoFunc::saveToRecent(const QString &appName, const QString &pathName)
{
    settingsManage *sm = settingsManage::initialize();
    QString m_appName = appName;

    if (sm->value("CoreApps", "KeepActivities")) {
        if (appName.count() && pathName.count()) {
            QSettings recentActivity(CPrime::Variables::CC_CoreApps_RecentActFilePath(), QSettings::IniFormat);
            QDateTime currentDT =  QDateTime::currentDateTime();
            QString group = currentDT.toString("dd.MM.yyyy");
            QString key = currentDT.toString("hh.mm.ss.zzz");
            recentActivity.beginGroup(group);
            recentActivity.setValue(key, m_appName + "\t\t\t" + pathName);
            recentActivity.endGroup();

            int count = recentActivity.childGroups().count();
            int limited = true;
            if (limited && count > 30) {
                QStringList list = SortFunc::sortDateTime(recentActivity.allKeys(), CPrime::Descending, "dd.MM.yyyy/hh.mm.ss.zzz");
                for (int i = count - 1; i >= 30; i--) {
                    recentActivity.remove(list[i]);
                }
            }
            return true;
        }
    }

    return false;
}
