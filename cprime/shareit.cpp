/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QIcon>
#include <QUrl>
#include <QStorageInfo>
#include <QListWidgetItem>
#include <QGuiApplication>
#include <QScreen>
#include <QPushButton>
#include <QPluginLoader>
#include <QDirIterator>
#include <QMimeData>
#include <QClipboard>

#include "ioprocesses.h"
#include "cvariables.h"
#include "appopenfunc.h"
#include "pinmanage.h"
#include "corexdg.h"
#include "themefunc.h"
#include "filefunc.h"

#include "shareit.h"
#include "ui_shareit.h"


shareit::shareit(QStringList files, QWidget *parent) : QDialog( nullptr ), ui(new Ui::shareit), mFiles(files), mParent(parent)
{
    ui->setupUi(this);

    resize(600,600);

    if (sm->value("CoreApps", "TouchMode"))
        if (not sm->value("CoreApps", "TabletMode"))
            setFixedSize(QGuiApplication::primaryScreen()->size() * .8 );

    setWindowFlags(Qt::WindowStaysOnTopHint );

    // Uniform Item Size, Grid Size etc
    QSize grid = (QSize)sm->value("CoreApps", "IconViewIconSize");
    ui->items->setGridSize( QSize( grid.width() * 2.2, grid.height() * 1.5 ) );

    // Hide the folders slist widget
    ui->folders->hide();

    ui->cancelBtn->setIconSize(sm->value("CoreApps", "ToolBarIconSize"));
    ui->items->setIconSize(sm->value("CoreApps", "IconViewIconSize"));
    ui->folders->setIconSize(sm->value("CoreApps", "ListViewIconSize"));
    ui->selectedFiles->setIconSize(sm->value("CoreApps", "IconViewIconSize"));
    ui->selectedFiles->setFixedHeight(ui->selectedFiles->iconSize().height()+30);
    ui->selectedL->setText(QString::number( files.count()) + " Files Selected");

    Q_FOREACH (QString file, mFiles) {
        QListWidgetItem *item = new QListWidgetItem(CPrime::FileUtils::baseName(file));
        item->setData(Qt::UserRole, Type::Folders);
        item->setIcon(CPrime::ThemeFunc::getFileIcon(file));
        ui->selectedFiles->addItem(item);
    }

    // add copy to clipbaord
    QListWidgetItem *itemcc = new QListWidgetItem("Copy to Clipboard");
    itemcc->setData(Qt::UserRole, Type::Plugins);
    itemcc->setData(Qt::UserRole + 1, "clipboard");
    itemcc->setIcon(QIcon::fromTheme( "edit-copy" ));
    itemcc->setText("Copy to Clipboard");
    ui->items->addItem(itemcc);

    // add copy to clipbaord
    QListWidgetItem *itemol = new QListWidgetItem("Open file location");
    itemol->setData(Qt::UserRole, Type::Plugins);
    itemol->setData(Qt::UserRole + 1, "location");
    itemol->setIcon(QIcon::fromTheme( "document-open" ));
    itemol->setText("Open file location");
    ui->items->addItem(itemol);


    populatePlugins();
    populateFavouriteApps();
    populateFolders();

    connect(ui->items, &QListWidget::itemClicked, this, &shareit::itemSelected);
    connect(ui->cancelBtn, &QPushButton::clicked, this, &QDialog::reject);
};

shareit::~shareit()
{
    Q_FOREACH (ShareItInterface *iface, plugins) {
        if (iface) {
            delete iface;
        }
    }

    delete ui;
}

void shareit::shareFiles(QWidget *parent, const QStringList files)
{
    shareit *dlg = new shareit(files, parent);
    dlg->exec();
};

void shareit::itemSelected(QListWidgetItem *item)
{
    Type t = item->data(Qt::UserRole).value<Type>();

    qDebug() << "Type " << t;
    qDebug() << "String " << item->data(Qt::UserRole + 1).toString();

	if (item->data(Qt::UserRole + 1).toString() == "clipboard") {
        copyToClipboard(item);
        return;
    }

	if (item->data(Qt::UserRole + 1).toString() == "location") {
		openFileLocation(item);
		return;
	}

    if (t == Type::Plugins) {
        plugin_itemActivated(item);
    } else if (t == Type::Apps) {
        apps_itemActivated(item);
    } else if (t == Type::Folders) {
        folders_itemActivated(item);
    }
}

void shareit::populateFavouriteApps()
{
    settingsManage *sm = settingsManage::initialize();
    QStringList apps = sm->value("CoreApps", "Favourite");

    Q_FOREACH (QString app, apps) {
        CoreDesktopFile file = CoreDesktopFile(app);
        QListWidgetItem *item = new QListWidgetItem(file.name());
        item->setData(Qt::UserRole, Type::Apps);
        item->setData(Qt::UserRole + 1, app);
        item->setIcon(CPrime::ThemeFunc::getAppIcon(file.icon()));
        item->setText(file.genericName());
        ui->items->addItem(item);
    }
}

void shareit::populatePlugins()
{
    QDir pluginsDir("/usr/lib/coreapps/shareit/");

    Q_FOREACH (QString pluginSo, pluginsDir.entryList(QStringList() << "*.so", QDir::Files, QDir::Name | QDir::IgnoreCase)) {
        QPluginLoader loader(pluginsDir.filePath(pluginSo));
        QObject *pObject = loader.instance();

        if (pObject) {
            ShareItInterface *iface = qobject_cast<ShareItInterface *>(pObject);

            /* iface is proper, we add the plugin */
            if (iface) {
                /* Check if the mimetypes match; check only first file */
                /* TODO: FIX this for multiple files later */
                QString firstMime = CPrime::FileUtils::mimeType( mFiles.at( 0 ) ).name();
                if ( iface->mimeTypes().contains( firstMime ) or iface->mimeTypes().contains( "*" ) ) {
                    QString name = iface->name();
                    QIcon icon = iface->icon();
                    QListWidgetItem *item = new QListWidgetItem(icon, name, ui->items);
                    item->setData(Qt::UserRole, Type::Plugins);
                    item->setData(Qt::UserRole + 1, plugins.count());                            // Index of the plugin in the list
                    plugins << iface;
                }
            } else {
                qWarning() << "Plugin Error:" << loader.errorString();
            }
        } else {
            qWarning() << "Plugin Error:" << loader.errorString();
        }
    }
}

void shareit::populateFolders()
{
    QIcon folder = QIcon::fromTheme("folder");

    // Speed Dial folders
    pinmanage pins;
    QStringList pinList = pins.getPinNames("Speed Dial");

    foreach (QString f, pinList) {
        QString path = pins.pinPath("Speed Dial", f);

        if (QFileInfo(path).isFile()) {
            continue;
        }

        QListWidgetItem *item = new QListWidgetItem(f);
        item->setData(Qt::UserRole, Type::Folders);
        item->setData(Qt::UserRole + 1, pins.pinPath("Speed Dial", f));
        item->setIcon(folder);
        ui->items->addItem(item);
    }

    // Home folders (Desktop, documents, downloads, music, pictures, videos)
    QStringList fList;
    fList << "Desktop" << "Documents" << "Downloads" << "Music" << "Pictures" << "Videos";
    QString homePath = QDir::homePath();

    Q_FOREACH (QString str, fList) {
        if(CPrime::FileUtils::isDir(homePath + "/" + str)){
            QListWidgetItem *item = new QListWidgetItem(str);
            item->setData(Qt::UserRole, Type::Folders);
            item->setData(Qt::UserRole + 1, homePath + "/" + str);
            item->setIcon(folder);
            ui->items->addItem(item);
        }
    }

    // Drives
    Q_FOREACH (QStorageInfo info, QStorageInfo::mountedVolumes()) {
        if (info.device() == QByteArray("tmpfs")) {
            continue;
        }

        QListWidgetItem *item = new QListWidgetItem(info.displayName());
        item->setData(Qt::UserRole, Type::Folders);
        item->setData(Qt::UserRole + 1, info.rootPath());
        item->setIcon(QIcon::fromTheme("drive-harddisk"));
        ui->items->addItem(item);
    }
}

void shareit::folders_itemActivated(QListWidgetItem *item)
{
    QString driveFolder = toDriveFolder(item->data(Qt::UserRole + 1).toString());
    qDebug() << driveFolder;

    if ( not driveFolder.isEmpty()) {
		CoreProcess::Process *process = new CoreProcess::Process;
		process->sourceDir = CPrime::FileUtils::dirName( mFiles.at( 0 ) );
		process->targetDir = driveFolder;

		if ( not process->sourceDir.endsWith( "/" ) )
			process->sourceDir += "/";

		if ( not process->targetDir.endsWith( "/" ) )
			process->targetDir += "/";

		QStringList srcList;
		for( QString file: mFiles ) {
			if ( CPrime::FileUtils::exists( file ) )
				srcList << file.replace( process->sourceDir, "" );
		}

		if ( not srcList.count() )
			return;

		process->type = CoreProcess::Copy;

		IODialog *pasteDlg = new IODialog( srcList, process );
		pasteDlg->show();
    }
}

void shareit::apps_itemActivated(QListWidgetItem *item)
{
    CPrime::AppOpenFunc::systemAppOpener(item->data(Qt::UserRole + 1).toString(), QStringList() << mFiles);
    this->close();
}

void shareit::plugin_itemActivated(QListWidgetItem *item)
{
    close();

    int index = item->data(Qt::UserRole + 1).toInt();
    plugins.at(index)->shareItDialog(mFiles, mParent);
};

QString shareit::toDriveFolder(QString driveMountPoint)
{
    ui->items->hide();
    ui->selectedFiles->hide();
    ui->title->hide();
    ui->selectedL->setText("Select a folder to copy");
    ui->folders->show();

    QListWidgetItem *item = nullptr;
    QIcon folder = QIcon::fromTheme("folder");
    QDirIterator iterator( driveMountPoint, QDir::Dirs | QDir::NoDotDot );

    while ( iterator.hasNext() ) {
        QString temp = iterator.next();
        item = new QListWidgetItem( temp );
        item->setData( Qt::UserRole + 1, temp );
        item->setIcon( folder );
        ui->folders->addItem( item );
    }


    bool touch = sm->value("CoreApps", "TouchMode");
    if(touch){
        connect(ui->folders, &QListWidget::itemClicked, [=](QListWidgetItem * item) {
            shareSelection = item->text();
            accept();
        });
    } else {
        connect(ui->folders, &QListWidget::itemDoubleClicked, [=](QListWidgetItem * item) {
            shareSelection = item->text();
            accept();
        });
    }

	/* Wait Loop for the user to make selection */
    QEventLoop *loop = new QEventLoop( this );
    connect( this, &QDialog::finished, loop, &QEventLoop::quit );

    loop->exec();

    return shareSelection;
}

void shareit::copyToClipboard(QListWidgetItem *item)
{
    Q_UNUSED(item)

	if (mFiles.count() == 1) {
		qDebug() << "Found One File...";
		QMimeDatabase mimedb;
		QString mtype = mimedb.mimeTypeForFile(mFiles.at(0)).name();
		mtype.resize(5);
		if (mtype == "image") {
			qDebug() << "Mime type image";
			QApplication::clipboard()->setPixmap(QPixmap(mFiles.at(0)));
			qDebug() << "Coping to clipboard";
			this->close();
			return;
		}
	}

    QList<QUrl> urlList;
    Q_FOREACH( QString item, mFiles )
        urlList << QUrl::fromLocalFile( item );

    QClipboard* clipboard = QApplication::clipboard();
    QMimeData* data = new QMimeData();

    QByteArray uriList;
    for (QUrl url : urlList) {
        uriList += url.toEncoded();
        uriList += "\r\n";
    }

    // Add current pid to trace cut/copy operations to current app
    // data->setData(QStringLiteral("text/x-libfmqt-pid"), ba.setNum(QCoreApplication::applicationPid()));
    // Gnome, LXDE, and XFCE
    // Note: the standard text/urilist format uses CRLF for line breaks, but gnome format uses LF only
    data->setData("x-special/gnome-copied-files", QByteArray("copy\n") + uriList.replace("\r\n", "\n"));
    // The KDE way
    data->setData("text/uri-list", uriList);
    // data->setData(QStringLiteral("application/x-kde-cutselection"), QByteArrayLiteral("0"));
    clipboard->setMimeData(data);

    this->close();
}

void shareit::openFileLocation(QListWidgetItem *item)
{
    Q_UNUSED(item)

	if (!mFiles.count()) {
		qDebug() << "No file"; // Just for checking
        return;
    }

    CPrime::AppOpenFunc::defaultAppEngine(CPrime::Category::FileManager,  CPrime::FileUtils::dirName( mFiles.at(0) ), "", this);
	this->close();
}
