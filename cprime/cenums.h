/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QObject>

namespace CPrime {

    enum AppsName {
        Nothing      = 0x0000,
        CoreAction   = 0x0001,
        CoreArchiver = 0x0002,
        CoreFM       = 0x0003,
        CoreGarage   = 0x0004,
        CoreHunt     = 0x0005,
        CoreImage    = 0x0006,
        CoreInfo     = 0x0007,
        CorePad      = 0x0008,
        CorePaint    = 0x0009,
        CorePDF      = 0x0010,
        CorePins     = 0x0011,
        CoreRenamer  = 0x0012,
        CoreShot     = 0x0013,
        CoreStats    = 0x0014,
        CoreSuff     = 0x0015,
        CoreTerminal = 0x0016,
        CoreTime     = 0x0017,
        CoreUniverse = 0x0018
    };

    enum Category {
        FileManager    = 0x0000,
        MetadataViewer = 0x0001,
        SearchApp      = 0x0002,
        ImageEditor    = 0x0003,
        Terminal       = 0x0004,
        Renamer        = 0x0005
    };

    enum SortOrder {
        Ascending    = 0x0000,
        Descending   = 0x0001
    };


    enum FileFolderSetup {
        ConfigFolder       = 0x0000,
        TrashFolder        = 0x0001
    };

}
