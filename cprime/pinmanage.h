/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include "cvariables.h"
#include "libcprime_global.h"


class LIBCPRIMESHARED_EXPORT pinmanage
{

public:
    bool addSection(QString sectionName);
    bool addPin(QString sectionName, QString pinName, QString pinpath);

    QStringList getPinSections();
    QStringList getPinNames(QString sectionName);
    QString piningTime(QString sectionName, QString pinName);
    QString pinPath(QString sectionName, QString pinName);
    QString checkingPinName(QString sectionName, QString pinName);
    QString checkingPinPath(QString section, QString pinPath);
    QString checkingPinPathEx(QString pinpath);

    void checkPins();
    void delSection(QString sectionName);
    void delPin(QString pinName);
    void changeAll(QString oldSectionName, QString oldpinName, QString sectionName, QString pinName, QString pinValue);
    void editPin(QString sectionName, QString pinName, QString pinpath);
    void delPin(QString pinName, QString section);

private:
	const QString cpinFullPath = CPrime::Variables::CC_CoreApps_PinsFilePath();

    void createPinsFolder();
    void changeSection(QString oldSectionName, QString sectionName, QString pinName, QString pinValue);
    void changePin(QString oldpinName, QString sectionName, QString pinName, QString pinValue);

    QStringList keys();
    QString pinValues(QString sectionName, QString pinName);
    QString keyCount();
};
