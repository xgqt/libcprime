/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDir>
#include <QSettings>

#include "settingsmanage.h"
#include "cvariables.h"


QString CPrime::Variables::CC_CoreApps_ConfigDir()
{
    return QFileInfo(CC_CoreApps_ConfigFilePath()).absolutePath();
}

QString CPrime::Variables::CC_CoreApps_Default_ConfigFilePath()
{
    return QString(QString(COREAPPS_CONFIG_FOLDER) + "/" + CC_CoreApps_ConfigFile());
}

QString CPrime::Variables::CC_CoreApps_ConfigFile()
{
    return QFileInfo(CC_CoreApps_ConfigFilePath()).fileName();
}

QString CPrime::Variables::CC_CoreApps_PinsFilePath()
{
    return QString(CC_CoreApps_ConfigDir() + "/" + CC_CoreApps_PinsFile());
}

QString CPrime::Variables::CC_CoreApps_ConfigFilePath()
{
    return QSettings("coreapps", "coreapps").fileName();
}

QString CPrime::Variables::CC_Home_TrashDir()
{
    return QString(QDir::homePath() + "/.local/share/Trash");
}

QString CPrime::Variables::CC_CoreApps_RecentActFilePath()
{
    return QString(CC_CoreApps_ConfigDir() + "/" + CC_CoreApps_RecentActFile());
}

QString CPrime::Variables::CC_CoreApps_SearchActFilePath()
{
    return QString(CC_CoreApps_ConfigDir() + "/" + CC_CoreApps_SearchActFile());
}

QString CPrime::Variables::CC_CoreApps_SessionFilePath()
{
    return QString(CC_CoreApps_ConfigDir() + "/" + CC_CoreApps_SessionFile());
}

QString CPrime::Variables::CC_CoreApps_NoteFilePath()
{
    return QString(CC_CoreApps_ConfigDir() + "/" + CC_CoreApps_NoteFile());
}
