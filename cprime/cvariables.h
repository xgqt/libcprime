/*
    *
    * This file is a part of Libcprime
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

// Returns coreaction plugin interface name
#define COREACTION_PLUGININTERFACE "org.coreapps.coreaction.plugininterface"

// Returns shareit plugin interface name
#define CPRIME_PLUGININTERFACE "org.coreapps.cprime.plugininterface"

// Returns the tasks plugin interface name
#define TASKS_PLUGININTERFACE "org.coreapps.corestuff.tasksplugin"

#include "libcprime_global.h"

namespace CPrime {

class LIBCPRIMESHARED_EXPORT Variables {

public:
    // Returns the Configuration folder path
    static QString CC_CoreApps_ConfigDir();

    // Returns the default configuration file path
    static QString CC_CoreApps_Default_ConfigFilePath();

    // Returns the config file
    static QString CC_CoreApps_ConfigFile();

    // Returns the config file path
    static QString CC_CoreApps_ConfigFilePath();

    // Returns the home trash folder path
    static QString CC_Home_TrashDir();

    // Returns the Pins file name
    static QString CC_CoreApps_PinsFile() {
        return "pins";
    }

    // Returns the Pins file path
    static QString CC_CoreApps_PinsFilePath();

    // Returns the Recent activity file
    static QString CC_CoreApps_RecentActFile() {
        return "recentactivity";
    }

    // Returns the Recent activity file path
    static QString CC_CoreApps_RecentActFilePath();

    // Returns the Search activity file
    static QString CC_CoreApps_SearchActFile() {
        return "searchactivity";
    }

    // Returns the Search Activity file path
    static QString CC_CoreApps_SearchActFilePath();

    // Returns the Session file
    static QString CC_CoreApps_SessionFile() {
        return "session";
    }

    // Returns the Session file
    static QString CC_CoreApps_SessionFilePath();

    // Returns the note file
    static QString CC_CoreApps_NoteFile() {
        return "note";
    }

    // Returns the note file path
    static QString CC_CoreApps_NoteFilePath();
};

}
