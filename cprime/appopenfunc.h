/*
    *
    * This file is a part of Libcprime.
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include "cenums.h"
#include "libcprime_global.h"

class QFileInfo;

namespace CPrime {

class LIBCPRIMESHARED_EXPORT AppOpenFunc {

public:
    static void systemAppOpener(const QString &appName, const QStringList &arg = QStringList(nullptr));
    static void appOpenEngine(const QString &path, QObject *processOwner);
    static void defaultAppEngine(CPrime::Category ctg, const QFileInfo &file, const QString keyword, QWidget *parent);

private:
    static QString setDefaultApp(QWidget *parent);
};

}
