/*
    *
    * This file is a part of Libcprime
    * Library for bookmarking, saving recent activites, managing settings for CuboCore Application Suite
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QDBusAbstractInterface>
#include <QDBusPendingCall>
#include <QDBusConnection>
#include <QDBusInterface>

#include "cenums.h"
#include "libcprime_global.h"


class LIBCPRIMESHARED_EXPORT NotifierInterface : public QDBusAbstractInterface {
	Q_OBJECT

public:
    NotifierInterface();
};

class CoreNotifier : public QObject {

    Q_OBJECT

public:
    static CoreNotifier* instance();

    /* Show a notification */
    void showNotification( QString appName, QString iconName, QString title, QString details, int timeout = 2000 );

private:
    CoreNotifier();

    NotifierInterface *iface;

    static CoreNotifier *mInstance;
    bool init;

private Q_SLOTS:
    void handleErrors( QDBusPendingCallWatcher* );

Q_SIGNALS:
    void notificationNotShown();
};
