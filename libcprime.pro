#-------------------------------------------------
#
# Project created by QtCreator 2018-06-20T09:12:54
#
#-------------------------------------------------

TEMPLATE = lib
TARGET   = cprime

QT      += widgets core gui dbus network

VERSION  = 4.0.0

# thread - Enable threading support
# silent - Do not print the compilation syntax
CONFIG  += thread silent

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Warn about deprecated features
DEFINES += QT_DEPRECATED_WARNINGS
# Disable all deprecated features before Qt 5.15
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051500

# Definetion section
DEFINES     += LIBCPRIME_LIBRARY

INCLUDEPATH += ./ ../
DEPENDPATH  += ./ ../

# Build section
MOC_DIR			= build/moc
OBJECTS_DIR		= build/obj
RCC_DIR			= build/qrc
UI_DIR			= build/uic

# C++17 Support for Qt5
QMAKE_CXXFLAGS += -std=c++17

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        # Must needed
        RESOURCE_FOLDER   = $$PREFIX/share/coreapps
        THEME_FOLDER      = $$RESOURCE_FOLDER/resource
        CONFIG_FOLDER     = $$THEME_FOLDER
        DEFINES          += COREAPPS_CONFIG_FOLDER=\"\\\"$${CONFIG_FOLDER}\\\"\"
        DEFINES          += COREAPPS_THEME_FOLDER=\"\\\"$${THEME_FOLDER}\\\"\"

        INSTALLS	 += target includes themefiles datafiles icons
        CONFIG		 += create_pc create_prl link_pkgconfig no_install_prl
        contains(DEFINES, LIB64): target.path = $$INSTALL_PREFIX/lib64
        else: target.path = $$INSTALL_PREFIX/lib

        target.path	  = $$PREFIX/lib/

        includes.files    = cprime/*.h libcprime_global.h cprime/application/capplication.h
        includes.path     = $$PREFIX/include/cprime/

        themefiles.path   = $$THEME_FOLDER
        themefiles.files  = resource/*.qss

        datafiles.path    = $$CONFIG_FOLDER
        datafiles.files   = resource/coreapps.conf

        icons.path        = /usr/share/icons/hicolor/scalable/apps/
        icons.files       = resource/applications-csuite.svg

        QMAKE_PKGCONFIG_FILE        = cprime
        QMAKE_PKGCONFIG_NAME        = libcprime
        QMAKE_PKGCONFIG_DESCRIPTION = Library for bookmarking, saving recent activites, managing settings of CoreApps.
        QMAKE_PKGCONFIG_PREFIX      = $$INSTALL_PREFIX
        QMAKE_PKGCONFIG_LIBDIR      = $$target.path
        QMAKE_PKGCONFIG_INCDIR      = $$includes.path
        QMAKE_PKGCONFIG_VERSION     = $$VERSION
        QMAKE_PKGCONFIG_DESTDIR     = pkgconfig # Destination Directory where pkgconfig installed
}


FORMS += \
    cprime/pinit.ui \
    cprime/shareit.ui

HEADERS += \
    cprime/settingsmanage.h \
    cprime/applicationdialog.h \
    cprime/shareit.h \
    cprime/cprime.h \
    cprime/themefunc.h \
    cprime/sortfunc.h \
    cprime/appopenfunc.h \
    cprime/infofunc.h \
    cprime/filefunc.h \
    cprime/trashmanager.h \
    cprime/cenums.h \
    cprime/cvariables.h \
    cprime/pinit.h \
    cprime/pinmanage.h \
    cprime/ioprocesses.h \
    cprime/cplugininterface.h \
    cprime/corexdg.h \
    cprime/application/capplication.h \
    cprime/application/qtlocalpeer.h \
    cprime/application/qtlockedfile.h \
    cprime/corenotifier.h

SOURCES += \
    cprime/settingsmanage.cpp \
    cprime/applicationdialog.cpp \
    cprime/shareit.cpp \
    cprime/themefunc.cpp \
    cprime/sortfunc.cpp \
    cprime/appopenfunc.cpp \
    cprime/infofunc.cpp \
    cprime/filefunc.cpp \
    cprime/trashmanager.cpp \
    cprime/cvariables.cpp \
    cprime/corexdg.cpp \
    cprime/pinit.cpp \
    cprime/pinmanage.cpp \
    cprime/ioprocesses.cpp \
    cprime/application/capplication.cpp \
    cprime/application/qtlocalpeer.cpp \
    cprime/corenotifier.cpp

RESOURCES += \
    resource.qrc
